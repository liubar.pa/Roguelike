using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    public class TriggerEnterChecker : MonoBehaviour
    {
        public void OnTriggerEnter2D(Collider2D collider)
        {
            CreateEntity(collider.gameObject);
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            CreateEntity(collision.gameObject);
        }

        /*Тут какая-то проблема. Начиная с какого-то момента он срабатывает каждый кадр,
        а не только при коллизии. Пока это не мешает, но потом надо будет поправить*/
        private void CreateEntity(GameObject other)
        {
            var world = General._world;
            int trigger_entity = world.NewEntity();
            int entity = this.GetComponent<EntityInfo>().entity;
            int other_entity = other.GetComponent<EntityInfo>().entity;
            General.AddComponentToEntity<TriggerEnterEvent>(world, trigger_entity, 
                new TriggerEnterEvent{_triggering_entity=entity,
                    _triggered_entity=other_entity});
        }
    }
}
