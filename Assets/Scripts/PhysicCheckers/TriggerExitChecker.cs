using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    public class TriggerExitChecker : MonoBehaviour
    {
        public void OnTriggerExit2D(Collider2D collider)
        {
            CreateEntity(collider.gameObject);
        }

        public void OnCollisionExit2D(Collision2D collision)
        {
            CreateEntity(collision.gameObject);
        }

        private void CreateEntity(GameObject other)
        {
            var world = General._world;
            int trigger_entity = world.NewEntity();
            int entity = this.GetComponent<EntityInfo>().entity;
            int other_entity = other.GetComponent<EntityInfo>().entity;
            General.AddComponentToEntity<TriggerExitEvent>(world, trigger_entity, 
                new TriggerExitEvent{_triggering_entity=entity,
                    _triggered_entity=other_entity});
        }
    }
}
