using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    public class TriggerStayChecker : MonoBehaviour
    {
        public void OnTriggerStay2D(Collider2D collider)
        {
            CreateEntity(collider.gameObject);
        }

        public void OnCollisionStay2D(Collision2D collision)
        {
            CreateEntity(collision.gameObject);
        }

        private void CreateEntity(GameObject other)
        {
            var world = General._world;
            int trigger_entity = world.NewEntity();
            int entity = this.GetComponent<EntityInfo>().entity;
            int other_entity = other.GetComponent<EntityInfo>().entity;
            General.AddComponentToEntity<TriggerStayEvent>(world, trigger_entity, 
                new TriggerStayEvent{_triggering_entity=entity,
                    _triggered_entity=other_entity});
        }
    }
}
