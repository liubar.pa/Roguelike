namespace Client
{
    /// Represents whole inventory UI
    struct InventoryUI
    {
	/// Slot for melee weapon (entity with component
	/// `InventorySlotUI`)
	public int _melee_slot;
	/// Slot   for   shooting   weapon  (entity   with   component
	/// `InventorySlotUI`)
	public int _shooting_slot;
	/// Slots for other items (entities with component
	/// `InventorySlotUI`)
	public int[] _storage_slots;
    }
}
