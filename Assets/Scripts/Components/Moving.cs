using UnityEngine;

namespace Client
{
    struct Moving
    {
        public Vector3 _direction;
        public float _speed;
    }
}