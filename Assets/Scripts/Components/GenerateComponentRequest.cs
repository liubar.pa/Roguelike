namespace Client
{
    struct GenerateComponentRequest<ComponentType> where ComponentType : struct
    {
        public int _sender_entity;
    }
}