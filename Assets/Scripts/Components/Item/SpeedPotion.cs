namespace Client
{
    /// Attached to speed potion item
    struct SpeedPotion
    {
	/// Speed multiplier
	public float _multiplier;
	/// Duration of speed buff
	public float _duration;
    }
}
