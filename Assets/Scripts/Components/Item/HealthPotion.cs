namespace Client
{
    /// Attached to health potion item
    struct HealthPotion
    {
	/// Health increase
	public float _health_increase;
    }
}
