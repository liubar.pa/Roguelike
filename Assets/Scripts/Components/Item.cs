using UnityEngine;

namespace Client
{
    /// Item
    struct Item
    {
	/// Name of item
	public string _name;
	/// Sprite of item
	public Sprite _sprite;
    }
}
