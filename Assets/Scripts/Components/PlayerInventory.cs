namespace Client
{
    /// Attached to player entity. Represents current inventory
    struct PlayerInventory
    {
	/// Melee weapon item (entity)
	public int? _melee;
	/// Shooting weapon item (entity)
	public int? _shooting;
	/// Items stored in inventory (entities)
	public int?[] _storage;
	/// Index of selected storage slot
	public int _selected_index;
	// Summary cost of all items
	public int _cost;
    }
}
