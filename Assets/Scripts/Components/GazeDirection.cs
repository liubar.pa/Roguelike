using UnityEngine;

namespace Client
{
    struct GazeDirection
    {
        public Vector3 _direction;
    }
}