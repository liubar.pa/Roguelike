namespace Client
{
    /// Trap decreasing entity's health
    struct FireTrap
    {
	/// Amount of damage dealt to entity per second
	public float _rate;
    }
}
