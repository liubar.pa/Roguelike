namespace Client
{
    /// Trap decreasing entity's speed
    struct FreezeTrap
    {
	/// Speed multiplier
	public float _multiplier;
    }
}
