namespace Client
{
    /// Request to drop item from player's inventory
    struct ItemDropRequest
    {
	/// Index of slot
	public int _slot_index;
    }
}
