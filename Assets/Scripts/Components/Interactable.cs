namespace Client
{
    /// Attached to entity which is possible to interact with
    struct Interactable
    {
	/// Max distance to interacting player
	public float _interaction_distance;
    }
}
