using UnityEngine;

namespace Client
{
    struct Shooting
    {
        public GameObject _projectile_prefab;
        public float _projectile_scale;
        public float _projectile_speed;
        public float _gaze_distance;
        public float _damage;
    }
}