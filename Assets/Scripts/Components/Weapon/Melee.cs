namespace Client
{
    struct Melee
    {
        public float _distance;

        public float _sector_angle; // in radians, 2 * pi is a maximum 
        public float _damage;
    }
}