using System.Collections.Generic;

namespace Client
{
    struct MapCleanRequest
    {
        public int _map_entity;
        public List<int> _entitys_of_nodes_to_be_spared; // those nodes won't be deleted
    }
}