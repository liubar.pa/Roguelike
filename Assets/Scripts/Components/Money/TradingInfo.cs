namespace Client
{
    struct TradingInfo
    {
        public MerchantMenu _menu;
        public TradingItem[] _slots;
        public int _merchant_entity;
    }
}