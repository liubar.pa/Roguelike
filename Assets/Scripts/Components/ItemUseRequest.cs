namespace Client
{
    /// Request to use item from player's inventory
    struct ItemUseRequest
    {
	/// Index of slot
	public int _slot_index;
    }
}
