namespace Client
{
    /// Request to add item to player's inventory
    struct ItemPickupRequest
    {
	/// Item to pick up (entity)
	public int _item;
    }
}
