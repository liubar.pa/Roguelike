using UnityEngine;

namespace Client
{
    struct GameObjectInfo
    {
        public GameObject _game_object;
        public Transform _transform;
    }
}