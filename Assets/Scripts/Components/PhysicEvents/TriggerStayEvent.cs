namespace Client
{
    struct TriggerStayEvent
    {
        public int _triggering_entity;
        public int _triggered_entity;
    }
}