namespace Client
{
    struct TriggerEnterEvent
    {
        public int _triggering_entity;
        public int _triggered_entity;
    }
}