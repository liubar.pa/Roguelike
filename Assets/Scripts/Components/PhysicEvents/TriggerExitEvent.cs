namespace Client
{
    struct TriggerExitEvent
    {
        public int _triggering_entity;
        public int _triggered_entity;
    }
}