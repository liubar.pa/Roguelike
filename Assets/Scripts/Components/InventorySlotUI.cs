using UnityEngine.UI;

namespace Client
{
    /// Represents inventory slot UI
    struct InventorySlotUI
    {
	/// Background of slot (used for highlighting selected slots)
	public Image _background;
	/// Item image
	public Image _item;
    }
}
