namespace Client
{
    /// Attached to entity getting steady health level change over time. 
    struct RegenerationBuff
    {
	/// Rate with which health is changed (health per second)
	public float _rate;
	/// Time left
	public float _time_left;
    }
}
