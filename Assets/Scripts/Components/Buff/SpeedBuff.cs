namespace Client
{
    /// Attached to player getting speed increase. Lasts for specific
    /// time
    struct SpeedBuff
    {
	/// Old speed of player
	public float _old_speed;
	/// New speed of player
	public float _new_speed;
	/// Time left
	public float _time_left;
    }
}
