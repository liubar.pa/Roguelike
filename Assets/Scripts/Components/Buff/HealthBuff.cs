namespace Client
{
    /// Attached to player getting health increase. One-shot
    struct HealthBuff
    {
	/// Health increase
	public float _health_increase;
    }
}
