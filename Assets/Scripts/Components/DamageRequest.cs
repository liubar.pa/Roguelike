namespace Client
{
    struct DamageRequest
    {
        public float _damage;
        public int _damaged_entity;
    }
}