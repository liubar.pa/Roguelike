namespace Client
{
    enum AIType
    { 
        Melee,
        Archer,
        Mage,
        Healer,
        Surrounding
    }

    struct AIBehaviourType
    {
        public AIType _type;
    }
}