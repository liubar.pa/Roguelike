using UnityEngine;
using System.Collections.Generic;

namespace Client
{
    enum LevelType
    {
        Neutral,
        Fire,
        Frosty,
        Market
    }

    struct MapGraphNode
    {
        public Vector2 _size;
        public LevelType _type;

        public List<int> _neigbours;
    }
}