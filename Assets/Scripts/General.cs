using System;
using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    sealed class General
    {
        static public EcsWorld _world;

        static public void AddComponentToEntity<ComponentType>(EcsWorld world, int entity, 
                                                               ComponentType component) 
                                                               where ComponentType: struct
        {
            world.GetPool<ComponentType>().Add(entity) = component;
        }

        static public void AddComponentToEntity<ComponentType>(EcsWorld world, int entity) 
                                                               where ComponentType: struct
        {
            world.GetPool<ComponentType>().Add(entity);
        }

        static private void GameObjectToEntity(EcsWorld world, GameObject obj, int entity)
        {
            var info = obj.AddComponent<EntityInfo>() as EntityInfo;
            info.entity = entity;
            AddComponentToEntity<GameObjectInfo>(world, entity, 
                new GameObjectInfo{_game_object=obj,
                    _transform=obj.transform});
        }

        static public GameObject Instantiate(EcsWorld world, GameObject prefab, Vector3 position,
                                       Quaternion rotation, int entity)
        {
            var obj = GameObject.Instantiate(prefab, position, rotation);
            GameObjectToEntity(world, obj, entity);
            return obj;
        }

        static public GameObject Instantiate(EcsWorld world, GameObject prefab, Transform parent,
                                         int entity)
        {
            var obj = GameObject.Instantiate(prefab, parent);
            GameObjectToEntity(world, obj, entity);
            return obj;
        }

        static public int
            GetFirstEntity<ComponentType>(EcsWorld world)
            where ComponentType: struct
        {
            foreach (var entity in world.Filter<ComponentType>().End())
            {
                return entity;
            }

            throw new Exception(
                $"No entities with component {typeof(ComponentType).Name} were found"
            );
        }

        static public ref ComponentType
            GetFirstComponent<ComponentType>(EcsWorld world)
            where ComponentType: struct
        {
            return ref world.GetPool<ComponentType>().Get(
                GetFirstEntity<ComponentType>(world)
            );
        }
    }
}
