using Leopotam.EcsLite;

namespace Client
{
    /// Extensions
    static class Extensions
    {
	/// Appends given array of systems to existing `IEcsSystems`
	public static IEcsSystems AddAll(this IEcsSystems systems,
					 IEcsSystem[] new_systems)
	{
	    foreach (IEcsSystem system in new_systems)
	    {
		systems.Add(system);
	    }

	    return systems;
	}
    }
}
