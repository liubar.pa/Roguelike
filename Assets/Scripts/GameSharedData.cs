namespace Client
{
    /// Data shared across all systems
    class GameSharedData {
	/// Game prefabs
	public GamePrefabs _prefabs;
	/// Game rules
	public GameRules _rules;
	/// Game sprites
	public GameSprites _sprites;
    }
}
