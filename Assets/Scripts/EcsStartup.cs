using Leopotam.EcsLite;
using Leopotam.EcsLite.Unity.Ugui;
using LeoEcsPhysics;
using UnityEngine;

namespace Client
{
    sealed class EcsStartup : MonoBehaviour
    {
        EcsWorld _world;        
        IEcsSystems _fixed_update_systems;
        IEcsSystems _update_systems;

	[SerializeField]
	GamePrefabs _prefabs;
	[SerializeField]
	GameRules _rules;

        [SerializeField]
        EcsUguiEmitter _ugui_emitter;

        void Start()
        {
            _world = new EcsWorld();
            General._world = _world;

	    GameSharedData data = new GameSharedData
	    {
		_prefabs = _prefabs,
		_rules = _rules,
		_sprites = new GameSprites()
	    };

            _fixed_update_systems = new EcsSystems(_world, data);
            _fixed_update_systems
                // register your systems here, for example:
                .Add(new GameInitSystem())
		.Add(new LevelGenerationSystem())
                .AddAll(PhysicsSystems.GetSystems())
                .Add(new AISystem())
                .Add(new MovingSystem())
                .Add(new ShootingSystem())
                .Add(new MeleeAttackSystem())
                .Add(new InteractionSystem())
                .Add(new DamagingSystem())
                .Add(new DeathSystem())
                .AddAll(ItemSystems.GetSystems())
                .AddAll(BuffSystems.GetSystems())
                .Add(new TradingUISystem())
                .Add(new InventorySystem())
                .Add(new InventoryUISystem())
		.Add(new HealthUISystem())
                .Add(new GarbageSystem())
#if UNITY_EDITOR
                // add debug systems for custom worlds here, for example:
                // .Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem("events"))
                .Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem())
#endif
                .InjectUgui(_ugui_emitter)
                .Init();


            _update_systems = new EcsSystems(_world, data);
            _update_systems
                .Add(new UpdatingCoordinatesSystem())
                .AddAll(InputSystems.GetSystems())
#if UNITY_EDITOR
                // add debug systems for custom worlds here, for example:
                // .Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem("events"))
                .Add(new Leopotam.EcsLite.UnityEditor.EcsWorldDebugSystem())
#endif
                .Init();
        }

        void FixedUpdate()
        {
            // process systems here.
            _fixed_update_systems?.Run();
        }

        void Update()
        {
            // process systems here.
            _update_systems?.Run();
        }

        void OnDestroy()
        {
            General._world = null;

            DestroySystems(_fixed_update_systems);
            DestroySystems(_update_systems);
            
            if (_world != null)
            {
                _world.Destroy();
                _world = null;
            }
        }

        void DestroySystems(IEcsSystems systems)
        {
            if (systems != null)
            {
                systems.Destroy();
                systems = null;
            }
        }
    }
}
