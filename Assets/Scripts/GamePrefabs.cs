using UnityEngine;

namespace Client
{
    /// Storage for all prefabs used in game
    [CreateAssetMenu(fileName = "GamePrefabs")]
    class GamePrefabs : ScriptableObject
    {
	/// Player prefab
	public GameObject _player_prefab;
	/// Projectile prefab
	public GameObject _projectile_prefab;
	/// Dropped item prefab
	public GameObject _item_prefab;
	/// Fire trap prefab
	public GameObject _fire_trap_prefab;
	/// Freeze trap prefab
	public GameObject _freeze_trap_prefab;
	/// Obstacle prefab
	public GameObject _obstacle_prefab;
	/// Inventory slot prefab
	public GameObject _slot_prefab;
	// Range zone prefab
	public GameObject _range_zone_prefab;
	//Trading UI
	public MerchantMenu _merchant_menu_prefab;
	public TradingItem _trading_item_prefab;
	public GameObject _merchant_prefab;
    }
}
