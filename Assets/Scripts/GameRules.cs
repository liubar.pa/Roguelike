using UnityEngine;

namespace Client
{
    /// General game rules
    [CreateAssetMenu(fileName = "GameRules")]
    class GameRules : ScriptableObject
    {
	/// Max distance from item to player trying to pick it up
	public float _item_interaction_distance;
	/// Time after which trap effect is reset
	public float _trap_decay;
    }
}
