using UnityEngine;
using Leopotam.EcsLite;

namespace Client
{
    class Layers {
        public static Vector3 basic_layer = Vector3.zero;
        public static Vector3 projectile_layer = Vector3.forward;
    };
}
