using System.Collections.Generic;
using UnityEngine;

namespace Client
{
    /// Loaded sprite cache
    class GameSprites
    {
	/// Cached sprites
	Dictionary<string, Sprite> _sprites;

	public GameSprites() {
	    _sprites = new Dictionary<string, Sprite>();
	}

	/// Returns sprite by name. Loads sprite if it was not loaded
	public Sprite GetSprite(string name)
	{
	    Sprite sprite;
	    if (_sprites.TryGetValue(name, out sprite))
	    {
		return sprite;
	    }

	    Sprite result = CreateSprite(name);
	    _sprites.Add(name, result);
	    return result;
	}

	/// Loads sprite with given name
	Sprite CreateSprite(string name)
	{
	    // TODO Actually load sprite

	    // Deterministic and random enough
	    int name_hash = name.GetHashCode();
	    Color color = new Color(
		(name_hash & 0xFF) / 255f,
		((name_hash >> 8) & 0xFF) / 255f,
		((name_hash >> 16) & 0xFF) / 255f,
		1
	    );

	    Texture2D texture = new Texture2D(1, 1);
	    texture.SetPixel(0, 0, color);
	    texture.Apply();

	    return Sprite.Create(
		texture, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f)
	    );
	}
    }
}
