using Leopotam.EcsLite;

namespace Client
{
    sealed class UpdatingCoordinatesSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();

            var entitys = world.Filter<Coordinate>().Inc<GameObjectInfo>().End();

            var coordinates_pool = world.GetPool<Coordinate>();
            var game_object_infos_pool = world.GetPool<GameObjectInfo>();

            foreach (var entity in entitys)
            {
                ref var coordinate = ref coordinates_pool.Get(entity);
                ref var game_object_info = ref game_object_infos_pool.Get(entity);
                
                game_object_info._transform.position = coordinate._coordinate;  
            }
        }
    }
}