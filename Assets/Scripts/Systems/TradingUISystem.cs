using Leopotam.EcsLite;
using Leopotam.EcsLite.Unity.Ugui;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace Client
{
    sealed class TradingUISystem : IEcsRunSystem
    {
        EcsUguiEmitter _ugui_emitter;
        Transform buying_inventory = null;

        Transform selling_inventory = null;
        

        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var data = systems.GetShared<GameSharedData>();
            var start_pool = world.GetPool<StartTradingRequest>();
            var start_filter = world.Filter<StartTradingRequest>().End();
            var end_filter = world.Filter<EndTradingRequest>().End();

            foreach (var entity in start_filter) {
                HandleStart(world, start_pool.Get(entity)._merchant_entity, data._prefabs);
                world.DelEntity(entity);
            }
            foreach (var entity in end_filter) {
                HandleEnd(world);
                world.DelEntity(entity);
            }
            var info_pool = world.GetPool<TradingInfo>();
            var info_filter = world.Filter<TradingInfo>().End();
            foreach (var entity in info_filter) {
                UpdateUI(world, ref info_pool.Get(entity));
            }
        }

        private void HandleStart(EcsWorld world, int merchant_entity, GamePrefabs prefabs)
        {
            ref var merchant = ref world.GetPool<Merchant>().Get(merchant_entity);
            ref var inventory = ref General.GetFirstComponent<PlayerInventory>(world);
            MerchantMenu menu = GameObject.Instantiate(prefabs._merchant_menu_prefab,
                Vector3.zero, Quaternion.identity);
            menu.transform.SetParent(_ugui_emitter.transform, false);
            selling_inventory = menu._selling_inventory.transform;
            buying_inventory = menu._buying_inventory.transform;
            UpdateCash(world, menu, merchant_entity);
            var capacity = inventory._storage.Length + merchant._storage.Length;
            TradingItem[] slots = new TradingItem[capacity];
            int index = 0;
            AddStorage(world, slots, inventory._storage, ref index, true, prefabs, merchant_entity);
            AddStorage(world, slots, merchant._storage, ref index, false, prefabs, merchant_entity);
            while (index < capacity) {
                slots[index] = null;
                ++index;
            }
            int entity = world.NewEntity();
            General.AddComponentToEntity(world, entity, new TradingInfo{
                _menu = menu,
                _slots = slots,
                _merchant_entity = merchant_entity
            });
        }

        private void AddStorage(EcsWorld world, TradingItem[] slots, int?[] storage, 
            ref int index, bool player_owner, GamePrefabs prefabs, int merchant)
        {
            var item_pool = world.GetPool<Item>();
            foreach (var item in storage) {
                if (item == null)
                    continue;
                slots[index] = GameObject.Instantiate(prefabs._trading_item_prefab,
                    Vector3.zero, Quaternion.identity);
                slots[index]._is_player_owner = player_owner;
                slots[index]._world = world;
                slots[index]._merchant_entity = merchant;
                slots[index]._item_entity = (int)item;
                slots[index]._name_text.text = item_pool.Get((int)item)._name;
                slots[index]._image.sprite = item_pool.Get((int)item)._sprite;
                SetDamage(world, slots[index]);
                UpdateSlot(world, slots[index]);
                ++index;
            }   
        }

        private void SetDamage(EcsWorld world, TradingItem slot)
        {
            int entity = slot._item_entity;
            var shooting_pool = world.GetPool<Shooting>();
            var melee_pool = world.GetPool<Melee>();
            float damage = 0;
            if (shooting_pool.Has(entity)) {
                damage = shooting_pool.Get(entity)._damage;
            } else if (melee_pool.Has(entity)) {
                damage = melee_pool.Get(entity)._damage;
            }
            slot._damage_text.text = "Урон: " + damage.ToString();
        }

        private void UpdateSlot(EcsWorld world, TradingItem slot)
        {
            int entity = slot._item_entity;
            var money_pool = world.GetPool<Money>();
            int price = money_pool.Get(entity)._money;
            if (slot._is_player_owner)
                price /= 2;
            slot._price_text.text = "Цена: " + price.ToString();
            if (slot._is_player_owner)
                slot.transform.SetParent(selling_inventory, false);
            else
                slot.transform.SetParent(buying_inventory, false);
        }

        private void UpdateCash(EcsWorld world, MerchantMenu menu, int merchant_entity)
        {
            var player = General.GetFirstEntity<Controlable>(world);
            var money_pool = world.GetPool<Money>();
            menu._player_money.text = "Деньги игрока: " + money_pool.Get(player)._money.ToString();
            menu._merchant_money.text = "Деньги торговца: " +
                money_pool.Get(merchant_entity)._money.ToString();
        }

        private void HandleEnd(EcsWorld world)
        {
            var info_entity = General.GetFirstEntity<TradingInfo>(world);
            ref var info = ref world.GetPool<TradingInfo>().Get(info_entity);
            GameObject.Destroy(info._menu.gameObject);
            world.DelEntity(info_entity);
            InputSystems._state = InputSystems.State.iBasic;
        }

        private void UpdateUI(EcsWorld world, ref TradingInfo info)
        {
            UpdateCash(world, info._menu, info._merchant_entity);
            ref var merchant = ref world.GetPool<Merchant>().Get(info._merchant_entity);
            ref var inventory = ref General.GetFirstComponent<PlayerInventory>(world);
            foreach (var slot in info._slots) {
                if (slot == null)
                    continue;
                foreach (var item in merchant._storage) {
                    if (item == null)
                        continue;
                    if (slot._item_entity == (int)item) {
                        if (slot._is_player_owner)
                            MoveSlot(world, slot);
                        break;
                    }
                }
                foreach (var item in inventory._storage) {
                    if (item == null)
                        continue;
                    if (slot._item_entity == (int)item) {
                        if (!slot._is_player_owner)
                            MoveSlot(world, slot);
                        break;
                    }
                }
            }
        }

        private void MoveSlot(EcsWorld world, TradingItem slot)
        {
            slot._is_player_owner = !slot._is_player_owner;
            UpdateSlot(world, slot);
        }
    }
}