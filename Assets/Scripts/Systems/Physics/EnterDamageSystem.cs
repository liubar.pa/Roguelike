using Leopotam.EcsLite;

namespace Client
{
    sealed class EnterDamageSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var triggers = world.Filter<TriggerEnterEvent>().End();
            var trigger_pool = world.GetPool<TriggerEnterEvent>();
            var health_pool = world.GetPool<Health>();
            var damaging_pool = world.GetPool<Damaging>();

            foreach (var entity in triggers) 
            {
                ref var trigger_info = ref trigger_pool.Get(entity);
                int from_entity = trigger_info._triggering_entity;
                int to_entity = trigger_info._triggered_entity;
                if (!health_pool.Has(to_entity) || !damaging_pool.Has(from_entity))
                    continue;
                var producer_pool = world.GetPool<ProducedBy>();
                if (producer_pool.Has(from_entity)) {
                    var producer = producer_pool.Get(from_entity)._producer_entity;
                    var controlable_pool = world.GetPool<Controlable>();
                    if (controlable_pool.Has(to_entity) == controlable_pool.Has(producer))
                        continue;
                }   
                int damage_entity = world.NewEntity();
                General.AddComponentToEntity<DamageRequest>(world, damage_entity, 
                    new DamageRequest{_damage=damaging_pool.Get(from_entity)._damage,
                        _damaged_entity=to_entity});
            }
        }
    }
}