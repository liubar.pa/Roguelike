using Leopotam.EcsLite;

namespace Client
{
    /// Helper class for obtaining all physics systems
    static class PhysicsSystems
    {
    /// Returns array of all physics systems
        public static IEcsSystem[] GetSystems()
        {
            return new IEcsSystem[]
            {
                new EnterDamageSystem(),
                new EnterDestroySystem(),
		new FireTrapSystem(),
		new FreezeTrapSystem(),
                new DeleteAllEventsSystem()
            };
        }
    }
}
