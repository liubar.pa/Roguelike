using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    sealed class EnterDestroySystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var triggers = world.Filter<TriggerEnterEvent>().End();
            var trigger_pool = world.GetPool<TriggerEnterEvent>();
            var destroy_pool = world.GetPool<DestroyAfterTrigger>();
            var game_object_pool = world.GetPool<GameObjectInfo>();

            foreach (var trigger in triggers) 
            {
                ref var trigger_info = ref trigger_pool.Get(trigger);
                int entity = trigger_info._triggering_entity;
                if (!destroy_pool.Has(entity))
                    continue;
                if (game_object_pool.Has(entity)) {
                    ref var info = ref game_object_pool.Get(entity);
                    GameObject.Destroy(info._game_object);
                    game_object_pool.Del(entity);
                }
                General.AddComponentToEntity(world, entity, new Garbage{});
            }
        }
    }
}