using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    /// System which processes fire trap triggering
    sealed class FireTrapSystem : IEcsRunSystem
    {
	public void Run(IEcsSystems systems) {
	    var world = systems.GetWorld();

	    var data = systems.GetShared<GameSharedData>();

	    var trigger_stay_event_pool = world.GetPool<TriggerStayEvent>();
	    var health_pool = world.GetPool<Health>();
	    var fire_trap_pool = world.GetPool<FireTrap>();

	    var trigger_events = world.Filter<TriggerStayEvent>().End();
	    foreach (var entity in trigger_events)
	    {
		ref var trigger_event = ref trigger_stay_event_pool
		    .Get(entity);

		int affected_entity = trigger_event._triggering_entity;
		int trap_entity = trigger_event._triggered_entity;

		if (health_pool.Has(affected_entity)
		    && fire_trap_pool.Has(trap_entity))
		{
		    HandleTrap(
			world, affected_entity, trap_entity,
			data._rules
		    );
		}
	    }
	}

	/// Handles fire trap effect on entity: updates entity's
	/// current regeneration buff (creates one if it does not
	/// exist)
	void HandleTrap(EcsWorld world, int affected_entity,
	                int trap_entity, GameRules rules)
	{
	    var fire_trap_pool = world.GetPool<FireTrap>();
	    var regeneration_buff_pool = world.GetPool<RegenerationBuff>();

	    ref var trap = ref fire_trap_pool.Get(trap_entity);

	    if (!regeneration_buff_pool.Has(affected_entity))
	    {
		General.AddComponentToEntity(
		    world, affected_entity, new RegenerationBuff
		    {
			_rate = -trap._rate,
			_time_left = 0
		    }
		);
	    }

	    ref var regeneration_buff = ref regeneration_buff_pool
		.Get(affected_entity);
	    regeneration_buff._time_left = rules._trap_decay;
	}
    }
}
