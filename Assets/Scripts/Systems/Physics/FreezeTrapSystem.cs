using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    /// System which processes freeze traps triggering
    sealed class FreezeTrapSystem : IEcsRunSystem
    {
	public void Run(IEcsSystems systems)
	{
	    var world = systems.GetWorld();

	    var data = systems.GetShared<GameSharedData>();

	    var trigger_stay_event_pool = world.GetPool<TriggerStayEvent>();
	    var moving_pool = world.GetPool<Moving>();
	    var freeze_trap_pool = world.GetPool<FreezeTrap>();

	    var trigger_events = world.Filter<TriggerStayEvent>().End();
	    foreach (var entity in trigger_events)
	    {
		ref var trigger_event = ref trigger_stay_event_pool
		    .Get(entity);

		int affected_entity = trigger_event._triggering_entity;
		int trap_entity = trigger_event._triggered_entity;

		if (moving_pool.Has(affected_entity)
		    && freeze_trap_pool.Has(trap_entity))
		{
		    HandleTrap(
			world, affected_entity, trap_entity,
			data._rules
		    );
		}
	    }
	}

	/// Handles freeze trap effect on entity: updates entity's
	/// current speed buff (creates one if it does not exist)
	void HandleTrap(EcsWorld world, int affected_entity,
	                int trap_entity, GameRules rules)
	{
	    var freeze_trap_pool = world.GetPool<FreezeTrap>();
	    var speed_buff_pool = world.GetPool<SpeedBuff>();
	    var moving_pool = world.GetPool<Moving>();

	    ref var trap = ref freeze_trap_pool.Get(trap_entity);

	    if (!speed_buff_pool.Has(affected_entity))
	    {
		float old_speed = moving_pool
		    .Get(affected_entity)
		    ._speed;
		General.AddComponentToEntity(
		    world, affected_entity, new SpeedBuff
		    {
			_old_speed = old_speed,
			_new_speed = 0,
			_time_left = 0
		    }
		);
	    }

	    ref var speed_buff = ref speed_buff_pool.Get(affected_entity);
	    speed_buff._new_speed =
		speed_buff._old_speed * trap._multiplier;
	    speed_buff._time_left = rules._trap_decay;
	}
    }
}
