using Leopotam.EcsLite;

namespace Client
{
    sealed class DeleteAllEventsSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            DeleteTriggers(world, world.Filter<TriggerEnterEvent>().End());
            DeleteTriggers(world, world.Filter<TriggerExitEvent>().End());
            DeleteTriggers(world, world.Filter<TriggerStayEvent>().End());
        }

        private void DeleteTriggers(EcsWorld world, EcsFilter triggers)
        {
            foreach (var entity in triggers) 
            {
                world.DelEntity(entity);
            }
        }
    }
}