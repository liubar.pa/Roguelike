using UnityEngine;
using UnityEditor;
using Leopotam.EcsLite;

namespace Client
{
    sealed class MeleeAttackSystem : IEcsRunSystem
    {
        /*
        You should find all entitys with component MeleeAttackRequest and perform attack of _attacking_entity.
        Attack can be performed by spawning special GameObject - range zone of weapon (sector of specified 
        angle and radius). This sector should have physics unity components to check collision. Collision events 
        will be processed by another system.
        */
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var attacking = world.Filter<MeleeAttackRequest>().End();
            var attack_request_pool = world.GetPool<MeleeAttackRequest>();
            var prefabs = systems.GetShared<GameSharedData>()._prefabs;

            foreach (var entity in attacking)
            {
                var attacking_entity = attack_request_pool.Get(entity)._attacking_entity;
                Attack(world, attacking_entity, prefabs);
                attack_request_pool.Del(entity);
            }
        }

        private ref Melee GetMelee(EcsWorld world, int entity)
        {
            if (world.GetPool<Melee>().Has(entity))
                return ref world.GetPool<Melee>().Get(entity);
            ref PlayerInventory inventory = ref world.GetPool<PlayerInventory>().Get(entity);
            return ref world.GetPool<Melee>().Get((int)inventory._melee);
        }

        private bool HasMelee(EcsWorld world, int entity)
        {
            if (world.GetPool<Melee>().Has(entity))
                return true;
            if (!world.GetPool<PlayerInventory>().Has(entity))
                return false;
            ref PlayerInventory inventory = ref world.GetPool<PlayerInventory>().Get(entity);
            return inventory._melee != null;
        }

        private void Attack(EcsWorld world, int entity, GamePrefabs prefabs)
        {
            if (!HasMelee(world, entity))
                return;
            ref var melee = ref GetMelee(world, entity);
            ref var coordinate = ref world.GetPool<Coordinate>().Get(entity);
            var range_zone_entity = world.NewEntity();

            var range_zone = General.Instantiate(world, prefabs._range_zone_prefab,
                coordinate._coordinate, Quaternion.identity, range_zone_entity);
            range_zone.AddComponent<TriggerEnterChecker>();
            var distance = melee._distance;
            if (world.GetPool<GazeDistance>().Has(entity))
                distance += world.GetPool<GazeDistance>().Get(entity)._gaze_distance;
            range_zone.transform.localScale *= distance;
            range_zone.transform.position += Layers.projectile_layer;
            

            General.AddComponentToEntity(world, range_zone_entity,
                new Damaging{_damage=melee._damage});
            
            General.AddComponentToEntity(world, range_zone_entity,
                new ProducedBy{_producer_entity=entity});

            General.AddComponentToEntity(world, range_zone_entity, new DestroyAfterTime{_time_left=2});
        }
    }
}