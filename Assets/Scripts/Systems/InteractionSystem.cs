using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    /// System obtains all `InteractionRequest`s and handles
    /// interactions (WIP).
    ///
    /// All `InteractionRequest`s are removed after running
    sealed class InteractionSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();
	    var interaction_request_pool = world
		.GetPool<InteractionRequest>();

	    var interaction_requests = world
		.Filter<InteractionRequest>().End();
	    foreach (var entity in interaction_requests)
	    {
		var interaction_request =
		    interaction_request_pool.Get(entity);
		HandleInteraction(world, ref interaction_request);
		interaction_request_pool.Del(entity);
	    }
        }

	/// Handles interaction request
	void HandleInteraction(EcsWorld world,
			       ref InteractionRequest request)
	{
	    if (GetClosestInteractable(
		    world, request._sender_entity
		) is int closest_interactable)
	    {
		HandleInteractable(
		    world, request._sender_entity,
		    closest_interactable
		);
	    }
	}

	/// Returns closest entity to given one which is possible to
	/// interact
	int? GetClosestInteractable(EcsWorld world, int sender_entity)
	{
	    var interactable_pool = world.GetPool<Interactable>();
	    var coordinate_pool = world.GetPool<Coordinate>();

	    Vector3 sender_position = coordinate_pool
		.Get(sender_entity)
		._coordinate;

	    int? closest_interactable = null;
	    float min_distance = float.PositiveInfinity;

	    var interactables = world
		.Filter<Interactable>().Inc<Coordinate>().End();
	    foreach (var interactable_entity in interactables)
	    {
		float interaction_distance = interactable_pool
		    .Get(interactable_entity)
		    ._interaction_distance;
		Vector3 position = coordinate_pool
		    .Get(interactable_entity)
		    ._coordinate;

		float distance = (position - sender_position).sqrMagnitude;

		if (distance <= interaction_distance * interaction_distance &&
		    distance < min_distance)
		{
		    min_distance = distance;
		    closest_interactable = interactable_entity;
		}
	    }

	    return closest_interactable;
	}

	/// Handles interaction with known interacting and interacted
	/// objects
	void HandleInteractable(EcsWorld world, int sender_entity,
				int interactable_entity)
	{
	    var item_pool = world.GetPool<Item>();
		var merchant_pool = world.GetPool<Merchant>();

	    if (item_pool.Has(interactable_entity))
	    {
		HandleItemInteraction(world, interactable_entity);
	    }
	    if (merchant_pool.Has(interactable_entity))
	    {
		HandleMerchant(world, interactable_entity);
	    }
	}

	/// Handles item interaction
	void HandleItemInteraction(EcsWorld world, int item_entity)
	{
	    var request_entity = world.NewEntity();
	    General.AddComponentToEntity(
		world, request_entity, new ItemPickupRequest
		{
		    _item = item_entity
		}
	    );
	}

	void HandleMerchant(EcsWorld world, int merchant_entity)
	{
		var request_entity = world.NewEntity();
		General.AddComponentToEntity(
		    world, request_entity, new StartTradingRequest
		    {
		        _merchant_entity = merchant_entity
		    }
	        );
		InputSystems._state = InputSystems.State.iMerchant;
	}
    }
}
