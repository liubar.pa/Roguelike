using System;
using System.Collections;
using System.Collections.Generic;
using Leopotam.EcsLite;
using UnityEngine;

using Random = UnityEngine.Random;

namespace Client
{
    sealed class LevelGenerationSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();
	    var data = systems.GetShared<GameSharedData>();

	    bool handled = false;
	    var level_switch_requests = world.Filter<LevelSwitchRequest>().End();
	    foreach (var entity in level_switch_requests)
	    {
		if (!handled)
		{
		    GenerateCurrentLevel(
			world, data._prefabs, data._rules
		    );
		    handled = true;
		}

		world.DelEntity(entity);
	    }
        }

        /*
        Generates current level from information in MapGraphNode and info about player.
        */
	void GenerateCurrentLevel(EcsWorld world, GamePrefabs prefabs,
				  GameRules rules)
	{
	    var node_pool = world.GetPool<MapGraphNode>();

	    ref var map = ref General.GetFirstComponent<Map>(world);
	    ref var node = ref node_pool.Get(map._current_node_entity);

	    GenerateLevel(world, ref node, prefabs, rules);
	}

	/// Generates level with use of given map node
        void GenerateLevel(EcsWorld world,
			   ref MapGraphNode node,
			   GamePrefabs prefabs,
			   GameRules rules)
        {
	    // TODO Set number of traps according to map size and game
	    // rules
	    switch (node._type)
	    {
	    case LevelType.Neutral:
		GenerateBattleLevel(
		    world, ref node, 4, 2, 2, prefabs, rules
		);
		break;
	    case LevelType.Fire:
		GenerateBattleLevel(
		    world, ref node, 4, 4, 0, prefabs, rules
		);
		break;
	    case LevelType.Frosty:
		GenerateBattleLevel(
		    world, ref node, 4, 0, 4, prefabs, rules
		);
		break;
	    case LevelType.Market:
		// TODO Market levels
		break;
	    }
        }

	void GenerateBattleLevel(EcsWorld world, ref MapGraphNode node,
				 int obstacle_count, int fire_trap_count,
				 int freeze_trap_count,
				 GamePrefabs prefabs,
				 GameRules rules)
	{
	    int tile_count = (int) (node._size.x * node._size.y);
	    int active_tile_count = obstacle_count + fire_trap_count
		+ freeze_trap_count;
	    int max_active_tile_count = tile_count - 2;

	    if (active_tile_count >= max_active_tile_count)
	    {
		throw new Exception(
		    "Max number of obstacles and traps was exceeded"
		);
	    }

	    // Step 1: obstacles

	    Vector2[] corners = new Vector2[]
	    {
		new Vector2(0, 0),
		new Vector2(0, node._size.y - 1),
		new Vector2(node._size.x - 1, node._size.y - 1),
		new Vector2(node._size.x - 1, 0)
	    };

	    bool[,] obstacles_draft = new bool[
		(int) node._size.y, (int) node._size.x
	    ];
	    List<Vector2> obstacles_queue = new List<Vector2>();
	    foreach (Vector2 corner in corners)
	    {
		obstacles_draft[(int) corner.y, (int) corner.x] = true;
		obstacles_queue.Add(corner);
	    }

	    bool[,] obstacles = new bool[
		(int) node._size.y, (int) node._size.x
	    ];

	    Vector2[] deltas = new Vector2[]
	    {
		new Vector2(0, -1), new Vector2(0, 1),
		new Vector2(-1, 0), new Vector2(1, 0)
	    };

	    int curr_obstacle_count = 0;
	    while (curr_obstacle_count < obstacle_count)
	    {
		int index = Random.Range(0, obstacles_queue.Count);
		++curr_obstacle_count;

		Vector2 curr = obstacles_queue[index];
		obstacles_queue[index] = obstacles_queue[obstacles_queue.Count - 1];
		obstacles_queue[obstacles_queue.Count - 1] = curr;

		obstacles_queue.RemoveAt(obstacles_queue.Count - 1);

		obstacles[(int) curr.y, (int) curr.x] = true;

		int obstacle_entity = world.NewEntity();

		General.Instantiate(
		    world, prefabs._obstacle_prefab,
		    new Vector3(curr.x, curr.y, 0),
		    Quaternion.identity, obstacle_entity
		);
		General.AddComponentToEntity(
		    world, obstacle_entity, new Obstacle{}
		);

		foreach (Vector2 delta in deltas)
		{
		    Vector2 next = curr + delta;
		    if (next.x >= 0 && next.x < node._size.x
			&& next.y >= 0 && next.y < node._size.y
			&& !obstacles_draft[(int) next.y, (int) next.x])
		    {
			obstacles_draft[(int) next.y, (int) next.x] = true;
			obstacles_queue.Add(next);
		    }
		}
	    }

	    // Step 2: traps

	    Vector2[] free_positions = new Vector2[tile_count - obstacle_count];
	    int free_position_count = 0;
	    for (int i = 0; i < node._size.y; ++i)
	    {
		for (int j = 0; j < node._size.x; ++j)
		{
		    if (!obstacles[j, i])
		    {
			free_positions[free_position_count] = new Vector2(j, i);
			++free_position_count;
		    }
		}
	    }

	    // Shuffling (Fisher--Yates algorithm)
	    for (int i = 0; i < free_position_count; ++i)
	    {
		int j = Random.Range(i, free_position_count);

		Vector2 temp = free_positions[i];
		free_positions[i] = free_positions[j];
		free_positions[j] = temp;
	    }

	    // First `fire_trap_count` are positions for fire traps
	    // Next `freeze_trap_count` are positions for freeze traps

	    for (int i = 0; i < fire_trap_count + freeze_trap_count; ++i)
	    {
		bool is_fire_trap = i < fire_trap_count;
		Vector2 position = free_positions[i];

		var trap_entity = world.NewEntity();

		General.Instantiate(
		    world,
		    is_fire_trap
		        ? prefabs._fire_trap_prefab
		        : prefabs._freeze_trap_prefab,
		    new Vector3(position.x, position.y, 0),
		    Quaternion.identity, trap_entity
		);

		if (is_fire_trap)
		{
		    // TODO Rate depending on player stats
		    General.AddComponentToEntity(
			world, trap_entity, new FireTrap{_rate = 100f}
		    );
		}
		else
		{
		    // TODO Multiplier depending on player stats
		    General.AddComponentToEntity(
			world, trap_entity, new FreezeTrap{_multiplier = 0.5f}
		    );
		}
	    }
	}
    }
}
