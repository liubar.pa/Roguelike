using UnityEngine;
using Leopotam.EcsLite;

namespace Client
{
    sealed class ShootingSystem : IEcsRunSystem
    {
        /*
        You should find all entitys with component ShootingRequest and perform shooting of _shooting_entity.
        Shooting can be performed by spawning special GameObject - projectile (circle with specified radius). 
        This circle should have physics unity components to check collision. Collision events will be processed
        by another system.
        */
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var shooting = world.Filter<ShootingRequest>().End();
            var shoot_request_pool = world.GetPool<ShootingRequest>();

            foreach (var entity in shooting)
            {
                var shooting_entity = shoot_request_pool.Get(entity)._shooting_entity;
                Shoot(world, shooting_entity);
                shoot_request_pool.Del(entity);
            }
        }

        private ref Shooting GetShooting(EcsWorld world, int entity)
        {
            if (world.GetPool<Shooting>().Has(entity))
                return ref world.GetPool<Shooting>().Get(entity);
            ref PlayerInventory inventory = ref world.GetPool<PlayerInventory>().Get(entity);
            return ref world.GetPool<Shooting>().Get((int)inventory._shooting);
        }

        private bool HasShooting(EcsWorld world, int entity)
        {
            if (world.GetPool<Shooting>().Has(entity)) 
                return true;
            if (!world.GetPool<PlayerInventory>().Has(entity))
                return false;
            ref PlayerInventory inventory = ref world.GetPool<PlayerInventory>().Get(entity);
            return inventory._shooting != null;
        }

        private void Shoot(EcsWorld world, int entity)
        {
            if (!HasShooting(world, entity)) 
                return;
            ref Shooting shooting = ref GetShooting(world, entity);
            ref var coordinate = ref world.GetPool<Coordinate>().Get(entity);
            ref var gaze = ref world.GetPool<GazeDirection>().Get(entity);
            var gaze_distance_pool = world.GetPool<GazeDistance>();

            var projectile_entity = world.NewEntity();

            var rotation = Quaternion.Euler(0, 0, Vector3.SignedAngle(Vector3.right,
                gaze._direction, new Vector3(0, 0, 1)));
            var projectile = General.Instantiate(world, shooting._projectile_prefab,
                coordinate._coordinate, rotation, projectile_entity);
            projectile.transform.localScale *= shooting._projectile_scale;
            var distance = shooting._gaze_distance;
            if (gaze_distance_pool.Has(entity))
                distance += gaze_distance_pool.Get(entity)._gaze_distance;
            projectile.transform.position += gaze._direction.normalized * distance
                + Layers.projectile_layer;

            General.AddComponentToEntity(world, projectile_entity,
                new Damaging{_damage=shooting._damage});
            
            General.AddComponentToEntity(world, projectile_entity,
                new ProducedBy{_producer_entity=entity});

            General.AddComponentToEntity(world, projectile_entity,
                new Moving{_direction=gaze._direction,
                    _speed=shooting._projectile_speed});

            General.AddComponentToEntity(world, projectile_entity,
                new Coordinate{_coordinate=projectile.transform.position});
            
            General.AddComponentToEntity(world, projectile_entity, 
                new DestroyAfterTrigger{});
        }
    }
}