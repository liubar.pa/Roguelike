using Leopotam.EcsLite;

namespace Client
{
    /// System obtains all `DeathRequest`s and:
    ///
    /// 1. Dispatches them based on type of died entity (player, enemy
    ///    or other);
    /// 2. Handles entities' deaths.
    ///
    /// All `DeathRequest`s are removed after running.
    sealed class DeathSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var death_request_pool = world.GetPool<DeathRequest>();

            var death_requests = world.Filter<DeathRequest>().End();
            foreach (var entity in death_requests)
            {
                var died_entity = death_request_pool.Get(entity)._died_entity;
                HandleEntityDeath(world, died_entity);
                world.DelEntity(entity);
            }
        }

        /// Determines type of died entity and handles its death
        void HandleEntityDeath(EcsWorld world, int entity)
        {
            var enemy_pool = world.GetPool<Enemy>();
            var controlable_pool = world.GetPool<Controlable>();

            // Entity type dispatching

            if (enemy_pool.Has(entity))
            {
                HandleEnemyDeath(world, entity);
            }
            else if (controlable_pool.Has(entity))
            {
                HandlePlayerDeath(world, entity);
            }
            else
            {
                HandleOtherEntityDeath(world, entity);
            }
        }

        /// Handles enemy death (WIP)
        void HandleEnemyDeath(EcsWorld world, int entity)
        {
            // TODO Implementation
            
            MoneyDrop(world, entity);
            world.GetPool<Garbage>().Add(entity);
        }

        void MoneyDrop(EcsWorld world, int entity)
        {
            var money_pool = world.GetPool<Money>();
            var player_entity = General.GetFirstEntity<Controlable>(world);
            if (money_pool.Has(entity) && money_pool.Has(player_entity)) {
                ref var player_money = ref money_pool.Get(player_entity);
                ref var enemy_money =  ref money_pool.Get(entity);
                player_money._money += enemy_money._money;
            }
        }

        /// Handles player death (WIP)
        void HandlePlayerDeath(EcsWorld world, int entity)
        {
            // TODO Implementation
        }

        /// Handles death of other entity (marks it as garbage)
        void HandleOtherEntityDeath(EcsWorld world, int entity)
        {
            world.GetPool<Garbage>().Add(entity);
        }
    }
}
