using UnityEngine;
using UnityEditor;
using UnityEngine.Assertions;

using Leopotam.EcsLite;

namespace Client
{
    
    sealed class GameInitSystem : IEcsInitSystem
    {

        private int CreateMelee(EcsWorld world, GameSprites sprites, float damage, int price)
        {
            int entity = world.NewEntity();
            General.AddComponentToEntity(world, entity, new Melee{_distance = 1, 
                                                                  _sector_angle = Mathf.PI / 4,
                                                                  _damage = damage});
            General.AddComponentToEntity(world, entity, new Weapon{});
            General.AddComponentToEntity(world, entity, new Money{_money=price});
            General.AddComponentToEntity(world, entity, new Item{
                _name="melee",
                _sprite=sprites.GetSprite("melee")
            });
            return entity;
        }

        private int CreateShooting(EcsWorld world, GamePrefabs prefabs,
				   GameSprites sprites, float damage, int price)
        {
            int entity = world.NewEntity();
	    var projectile_prefab = prefabs._projectile_prefab;
            General.AddComponentToEntity(world, entity,
                new Shooting{
                    _projectile_prefab = projectile_prefab,
                    _projectile_scale = 1f,
                    _projectile_speed = 0.5f,
                    _gaze_distance = 0.15f,
                    _damage = damage
                    });
            General.AddComponentToEntity(world, entity, new Money{_money=price});
            General.AddComponentToEntity(world, entity, new Weapon{});
            General.AddComponentToEntity(world, entity, new Item{
                _name="shooting",
                _sprite=sprites.GetSprite("shooting")
            });
            return entity;
        }

        public int CreateMerchant(EcsWorld world, GameSprites sprites, GamePrefabs prefabs, 
            int storage_size, int item_count, int money, Vector3 position)
        {
            int entity = world.NewEntity();
            var storage = new int?[storage_size];
            General.AddComponentToEntity(world, entity, new Money{_money=money});
            General.AddComponentToEntity(world, entity, new Interactable{_interaction_distance=3});
            General.Instantiate(world, prefabs._merchant_prefab, position, 
                Quaternion.identity, entity);
            General.AddComponentToEntity(world, entity, new Coordinate{_coordinate=position});
            for (int i = 0; i < item_count; ++i) {
                storage[i] = CreateMelee(world, sprites, i, i * 10);
            }
            General.AddComponentToEntity(world, entity, 
                new Merchant{_storage=storage});
            return entity;
        }

        public int CreateEnemy(EcsWorld world, GameObject prefab)
        {
            int entity = world.NewEntity();
            General.Instantiate(world, prefab, new Vector3(5, 0, 0), Quaternion.identity, entity);
            General.AddComponentToEntity(world, entity, new Health{_health = 5f});
            General.AddComponentToEntity(world, entity, 
                                         new Coordinate{_coordinate = new Vector3(5f, 0f, 0f)});
            General.AddComponentToEntity(world, entity, new Money{_money=100});
            General.AddComponentToEntity(world, entity, new Enemy{});
            return entity;
        }

        public void Init(IEcsSystems systems)
        {
            var world = systems.GetWorld();
	    var data = systems.GetShared<GameSharedData>();

	    var player_prefab = data._prefabs._player_prefab;
            
            CreateEnemy(world, player_prefab);

            var player_entity = world.NewEntity();

            var player_game_object = General.Instantiate(world, player_prefab, 
                                                         new Vector3(0f, 0f, 0f),
                                                         Quaternion.identity,
                                                         player_entity);

	    player_game_object.AddComponent<TriggerStayChecker>();

            General.AddComponentToEntity(world, player_entity, 
                                         new Coordinate{_coordinate = new Vector3(0f, 0f, 0f)});

            General.AddComponentToEntity(world, player_entity, new Moving{_direction = Vector3.zero, _speed = 0.2f});

            General.AddComponentToEntity(world, player_entity, 
                                         new GazeDirection{_direction = new Vector3(1, 0, 0)});

            General.AddComponentToEntity(world, player_entity, new Health{_health = 500f});
            
            General.AddComponentToEntity(world, player_entity, new GazeDistance{_gaze_distance = 1f});

	    General.AddComponentToEntity(world, player_entity, new Interacting{});

	    General.AddComponentToEntity(
		world, player_entity,
		new PlayerInventory
		{
		    _melee = CreateMelee(world, data._sprites, 1, 10),
		    _shooting = CreateShooting(world, data._prefabs, data._sprites, 1, 20),
		    _storage = new int?[10],
		    _selected_index = 0,
                    _cost = 0
		}
	    );

            General.AddComponentToEntity<Controlable>(world, player_entity);
            General.AddComponentToEntity(world, player_entity, new Money{_money=50});  
        }
    }
}
