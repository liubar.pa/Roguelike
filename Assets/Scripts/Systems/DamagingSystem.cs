using System;
using Leopotam.EcsLite;

namespace Client
{
    /// System obtains all `DamageRequest`s and:
    ///
    /// 1. Handles health decreasing;
    /// 2. Triggers death requests if necessary.
    ///
    /// All `DamageRequest`s are removed after running.
    sealed class DamagingSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();
	    var damage_request_pool = world.GetPool<DamageRequest>();

	    var damage_requests = world
		.Filter<DamageRequest>().End();
	    foreach (var entity in damage_requests)
	    {
		var damage_request = damage_request_pool.Get(entity);
		HandleDamageRequest(world, damage_request);
		damage_request_pool.Del(entity);
	    }
        }

	void HandleDamageRequest(EcsWorld world, DamageRequest request)
	{
	    var health_pool = world.GetPool<Health>();
	    ref var health = ref health_pool.Get(request._damaged_entity);

	    float old_health = health._health;
	    float new_health = Math.Max(old_health - request._damage, 0);

	    if (old_health <= 0)
	    {
		return;
	    }

	    health._health = new_health;
	    if (new_health == 0)
	    {
		var death_request = world.NewEntity();
		General.AddComponentToEntity(
		    world, death_request,
		    new DeathRequest{_died_entity = request._damaged_entity}
		);
	    }
	}
    }
}
