using Leopotam.EcsLite;
using Leopotam.EcsLite.Unity.Ugui;
using TMPro;

namespace Client
{
    /// System which deals with health text UI
    sealed class HealthUISystem : IEcsRunSystem
    {
	EcsUguiEmitter _ugui_emitter;

	[EcsUguiNamed("HealthText")]
	TMP_Text _health_text;

	public void Run(IEcsSystems systems)
	{
	    var world = systems.GetWorld();

	    var health_pool = world.GetPool<Health>();

	    var player_entity = General.GetFirstEntity<Controlable>(world);
	    ref var health = ref health_pool.Get(player_entity);

	    _health_text.text = $"Health: {health._health}";
	}
    }
}
