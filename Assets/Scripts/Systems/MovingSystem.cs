using UnityEngine;

using Leopotam.EcsLite;

namespace Client
{
    sealed class MovingSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();

            MoveEntitysWithGazeDirection(world);
            MoveEntitysWithoutGazeDirection(world);
        }

        private void MoveEntitysWithoutGazeDirection(EcsWorld world)
        {
            var entitys_to_move = world.Filter<Moving>().Inc<Coordinate>().Exc<GazeDirection>().End();

            foreach (var entity in entitys_to_move)
            {
                MoveEntity(world, entity);  
            }
        }

        private void MoveEntitysWithGazeDirection(EcsWorld world)
        {
            var entitys_to_move = world.Filter<Moving>().Inc<Coordinate>().Inc<GazeDirection>().End();

            foreach (var entity in entitys_to_move)
            {
                MoveEntity(world, entity);
                ChangeGazeDirection(world, entity);
            }
        }

        private void MoveEntity(EcsWorld world, int entity)
        {
            var coordinates_pool = world.GetPool<Coordinate>();
            var movings_pool = world.GetPool<Moving>();

            ref var coordinate = ref coordinates_pool.Get(entity);
            ref var moving = ref movings_pool.Get(entity);  

            if (moving._direction != Vector3.zero)
                coordinate._coordinate += (Vector3)moving._direction * moving._speed; 
        }

        private void ChangeGazeDirection(EcsWorld world, int entity)
        {
            var movings_pool = world.GetPool<Moving>();
            var gaze_directions_pool = world.GetPool<GazeDirection>();

            ref var moving = ref movings_pool.Get(entity);
            ref var gaze_direction = ref gaze_directions_pool.Get(entity); 

            if (moving._direction != Vector3.zero)
                gaze_direction._direction = (Vector3)moving._direction;
        }
    }
}