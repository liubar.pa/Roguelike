using Leopotam.EcsLite;
using System;
using UnityEngine;

namespace Client
{
    sealed class MerchantInputSystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            if (InputSystems._state != InputSystems.State.iMerchant)
                return;
            var world = systems.GetWorld();
            HandleExit(world);
        }

        private void HandleExit(EcsWorld world)
        {
            if (Input.GetButtonDown("Cancel"))
            {
                var request_entity = world.NewEntity();
                General.AddComponentToEntity(
                world, request_entity, new EndTradingRequest{});
            }
        }
    }
}