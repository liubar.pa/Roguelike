using Leopotam.EcsLite;
using System;
using UnityEngine;

namespace Client
{
    // TODO Move to new InputSystem
    sealed class InputHandlingSystem : IEcsRunSystem
    {
        /*
        Input handling should be performed this way: you should process entitys with Controlable component, 
        WASD is for movement, on press those keys changes Moving component of entity. LMB is for melee attack, 
        on press you should spawn entity with Component MeleeAttackRequest for MeleeAttackSystem to process. 
        RMB is for shooting, on press you should spawn entity with component ShootingRequest for ShootingSystem 
        to process.
        */
        public void Run(IEcsSystems systems)
        {
            if (InputSystems._state != InputSystems.State.iBasic)
                return;
            var world = systems.GetWorld();
            var moving_entities = world.Filter<Controlable>().Inc<Moving>().End();
            ManageMovingEntites(world, moving_entities);

            var melee_entities = world.Filter<Controlable>().End();
            ManageMeleeEntities(world, melee_entities);

            var shooting_entities = world.Filter<Controlable>().End();
            ManageShootingEntities(world, shooting_entities);

	    var interacting_entities = world.Filter<Controlable>().Inc<Interacting>().End();
	    ManageInteractingEntities(world, interacting_entities);

	    ManageInventory(world);
        }

        private void ManageMovingEntites(EcsWorld world, EcsFilter moving_entities)
        {
            var moving_pool = world.GetPool<Moving>();

            var direction = HandleMovementButtons();

            foreach (var entity in moving_entities)
            {
                ref var moving_component = ref moving_pool.Get(entity);
                moving_component._direction = direction;
            }
        }

        private Vector3 HandleMovementButtons()
        {
            Vector3 direction = Vector3.zero;
            direction.x += Input.GetAxis("Horizontal");
            direction.y += Input.GetAxis("Vertical");

            direction.Normalize();
            return direction;
        }

        private void ManageMeleeEntities(EcsWorld world, EcsFilter melee_entities)
        {
            if (Input.GetButtonDown("MeleeAttack")) //Missing presses in fixed update
            {
                foreach (var entity in melee_entities)
                {
                    var attack_request = world.NewEntity();
                    General.AddComponentToEntity(world, attack_request, new MeleeAttackRequest{_attacking_entity = entity});
                }
            }
        }

        private void ManageShootingEntities(EcsWorld world, EcsFilter shooting_entities)
        {
            if (Input.GetButtonDown("Shoot"))
            {
                foreach (var entity in shooting_entities)
                {
                    var shoot_request = world.NewEntity();
                    General.AddComponentToEntity(world, shoot_request, new ShootingRequest{_shooting_entity = entity});
                }
            }
        }

	private void ManageInteractingEntities(EcsWorld world, EcsFilter interacting_entities)
	{
	    if (Input.GetButtonDown("Interact"))
	    {
		foreach (var entity in interacting_entities)
		{
		    var interact_request = world.NewEntity();
		    General.AddComponentToEntity(world, interact_request, new InteractionRequest{_sender_entity = entity});
		}
	    }
	}

	private void ManageInventory(EcsWorld world)
	{
	    // Getting state
	    ref var inventory = ref General
		.GetFirstComponent<PlayerInventory>(world);
	    ref int index = ref inventory._selected_index;
	    int capacity = inventory._storage.Length;

	    // Handling item usage

	    if (Input.GetButtonDown("Use"))
	    {
		var request_entity = world.NewEntity();
		General.AddComponentToEntity(
		    world, request_entity, new ItemUseRequest
		    {
			_slot_index = index
		    }
		);
	    }

	    // Handling item dropping

	    if (Input.GetButtonDown("Drop"))
	    {
		var request_entity = world.NewEntity();
		General.AddComponentToEntity(
		    world, request_entity, new ItemDropRequest
		    {
			_slot_index = index
		    }
		);
	    }

	    // Handling slot selection

	    float scroll = Input.GetAxisRaw("Mouse ScrollWheel");

	    int slot_delta = scroll switch {
		< 0 => 1,
		> 0 => -1,
		_ => 0
	    };

	    index = (index + slot_delta + capacity) % capacity;
	}
    }
}
