using Leopotam.EcsLite;

namespace Client
{
    /// Helper class for obtaining all input systems
    static class InputSystems
    {
        public enum State {iBasic, iMerchant};
        static public State _state = State.iBasic;

        /// Returns array of all input systems
        public static IEcsSystem[] GetSystems()
        {
            return new IEcsSystem[]
            {
                new InputHandlingSystem(),
                new MerchantInputSystem()
            };
        }
    }
}
