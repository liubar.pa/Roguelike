using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    /// System which processes items' dropping and picking up
    ///
    /// All `ItemDropRequest`s, `ItemPickupRequest`s and
    /// `ItemUseRequest`s are removed after running
    sealed class InventorySystem : IEcsRunSystem
    {
        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            var data = systems.GetShared<GameSharedData>();
            var inventory_pool = world.GetPool<PlayerInventory>();

            int player_entity = General.GetFirstEntity<Controlable>(world);
            ref var inventory = ref inventory_pool.Get(player_entity);

            HandleDropRequests(world, data, player_entity, ref inventory);
            HandlePickupRequests(world, player_entity, ref inventory);
            HandleUseRequests(world, player_entity, ref inventory);
            HandleBuyingRequests(world, player_entity, ref inventory);
            HandleSellingRequests(world, player_entity, ref inventory);
        }

        /// Handles all item dropping requests and removes them
        void HandleDropRequests(EcsWorld world, GameSharedData data,
                    int player_entity,
                    ref PlayerInventory inventory)
        {
            var money_pool = world.GetPool<Money>();
            var drop_request_pool = world.GetPool<ItemDropRequest>();

            Vector3 position = world
                .GetPool<Coordinate>()
                .Get(player_entity)
                ._coordinate;

            var drop_requests = world.Filter<ItemDropRequest>().End();
            foreach (var entity in drop_requests)
            {
                int slot_index = drop_request_pool.Get(entity)._slot_index;

                if (inventory._storage[slot_index] != null)
                {
                    var item = (int)inventory._storage[slot_index];
                    if (money_pool.Has(item))
                        inventory._cost -= money_pool.Get(item)._money;
                    ItemUtils.MakeItemDropped(
                        world,
                        data._prefabs,
                        data._rules,
                        (int)inventory._storage[slot_index],
                        position
                    );
                    inventory._storage[slot_index] = null;
                }

                drop_request_pool.Del(entity);
            }
        }

        /// Handles all item pick up requests and removes them
        void HandlePickupRequests(EcsWorld world, int player_entity,
                    ref PlayerInventory inventory)
        {
            var money_pool = world.GetPool<Money>();
            var pickup_request_pool = world.GetPool<ItemPickupRequest>();

            int selected_index = inventory._selected_index;

            var pickup_requests = world.Filter<ItemPickupRequest>().End();
            foreach (var entity in pickup_requests)
            {
                int item = pickup_request_pool.Get(entity)._item;

                if (inventory._storage[selected_index] == null)
                {
                    ItemUtils.MakeItemPickedUp(world, item);
                    inventory._storage[selected_index] = item;
                    if (money_pool.Has(item))
                        inventory._cost += money_pool.Get(item)._money;
                }

                pickup_request_pool.Del(entity);
            }
        }

        /// Handles generic part of all item use requests: removing
        /// used items and removing requests
        void HandleUseRequests(EcsWorld world, int player_entity,
                    ref PlayerInventory inventory)
        {
            var item_use_request_pool = world.GetPool<ItemUseRequest>();
            var item_remove_after_use_pool = world
                .GetPool<ItemRemoveAfterUse>();

            var item_use_requests = world
                .Filter<ItemUseRequest>().End();
            foreach (var entity in item_use_requests)
            {
                int slot_index = item_use_request_pool
                    .Get(entity)
                    ._slot_index;
                ref int? item = ref inventory._storage[slot_index];

                if (item is int item_entity &&
                    item_remove_after_use_pool.Has(item_entity)) 
                {
                    world.DelEntity(item_entity);
                    item = null;
                }

                item_use_request_pool.Del(entity);
            }
        }

        void HandleBuyingRequests(EcsWorld world, int player_entity,
                    ref PlayerInventory inventory)
        {
            var buying_pool = world.GetPool<BuyingRequest>();
            var merchant_pool = world.GetPool<Merchant>();
            var money_pool = world.GetPool<Money>();
            var buying_entities = world.Filter<BuyingRequest>().End();
            foreach (var request_entity in buying_entities) 
            {
                General.AddComponentToEntity<Garbage>(world, request_entity,
                    new Garbage{});
                ref var request = ref buying_pool.Get(request_entity);
                ref var merchant = ref merchant_pool.Get(request._merchant_entity);
                var item = request._item_entity;

                var index = -1;
                var buying_index = -1;
                for (int i = 0; i < inventory._storage.Length; ++i)
                {
                    if (inventory._storage[i] == null) {
                        index = i;
                        break;
                    }
                }
                for (int i = 0; i < merchant._storage.Length; ++i)
                {
                    if (merchant._storage[i] == item) {
                        buying_index = i;
                        break;
                    }
                }
                if (index == -1 || buying_index == -1)
                    continue;
                
                ref var player_money = ref money_pool.Get(player_entity)._money;
                ref var merchant_money = ref money_pool.Get(request._merchant_entity)._money;
                ref var item_money = ref money_pool.Get(item)._money;
                if (player_money < item_money)
                    continue;
                player_money -= item_money;
                merchant_money += item_money;
                inventory._storage[index] = item;
                merchant._storage[buying_index] = null;
            }
        }

        void HandleSellingRequests(EcsWorld world, int player_entity,
                    ref PlayerInventory inventory)
        {
            const float SELLING_KF = 0.5f;
            var selling_pool = world.GetPool<SellingRequest>();
            var merchant_pool = world.GetPool<Merchant>();
            var money_pool = world.GetPool<Money>();
            var selling_entities = world.Filter<SellingRequest>().End();
            foreach (var request_entity in selling_entities) 
            {
                General.AddComponentToEntity<Garbage>(world, request_entity,
                    new Garbage{});
                ref var request = ref selling_pool.Get(request_entity);
                ref var merchant = ref merchant_pool.Get(request._merchant_entity);
                var item = request._item_entity;

                var index = -1;
                var selling_index = -1;
                for (int i = 0; i < merchant._storage.Length; ++i)
                {
                    if (merchant._storage[i] == null) {
                        index = i;
                        break;
                    }
                }
                for (int i = 0; i < inventory._storage.Length; ++i)
                {
                    if (inventory._storage[i] == item) {
                        selling_index = i;
                        break;
                    }
                }
                if (index == -1 || selling_index == -1)
                    continue;
                
                ref var player_money = ref money_pool.Get(player_entity)._money;
                ref var merchant_money = ref money_pool.Get(request._merchant_entity)._money;
                var item_money = (int)(money_pool.Get(item)._money * SELLING_KF);
                if (merchant_money < item_money)
                    continue;
                player_money += item_money;
                merchant_money -= item_money;
                merchant._storage[index] = item;
                inventory._storage[selling_index] = null;
            }
        }
    }
}
