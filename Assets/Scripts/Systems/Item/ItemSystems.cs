using Leopotam.EcsLite;

namespace Client
{
    /// Helper class for obtaining all item systems
    static class ItemSystems
    {
	/// Returns array of all item systems
	public static IEcsSystem[] GetSystems()
	{
	    return new IEcsSystem[]
	    {
		new HealthPotionSystem(),
		new SpeedPotionSystem()
	    };
	}
    }
}
