using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    /// System which processes the usage of health potions by the
    /// player
    sealed class HealthPotionSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();
	    var inventory_pool = world.GetPool<PlayerInventory>();
	    var item_use_request_pool = world.GetPool<ItemUseRequest>();
	    var health_potion_pool = world.GetPool<HealthPotion>();
	    
	    int player_entity = General.GetFirstEntity<Controlable>(world);
	    ref var inventory = ref inventory_pool.Get(player_entity);

	    var item_use_requests = world
		.Filter<ItemUseRequest>().End();
	    foreach (var entity in item_use_requests)
	    {
		int slot_index = item_use_request_pool
		    .Get(entity)
		    ._slot_index;
		int? item = inventory._storage[slot_index];

		if (item is int item_entity &&
		    health_potion_pool.Has(item_entity))
		{
		    HandleHealthPotion(
			world, player_entity, item_entity
		    );
		}
	    }
        }

	/// Handles health potion usage: adds required amount of
	/// health increase to player's current health buff (creates
	/// one if it does not exist)
	void HandleHealthPotion(EcsWorld world, int player_entity,
				int item_entity)
	{
	    var health_potion_pool = world.GetPool<HealthPotion>();
	    var health_buff_pool = world.GetPool<HealthBuff>();

	    ref var health_potion = ref health_potion_pool
		.Get(item_entity);

	    if (!health_buff_pool.Has(player_entity))
	    {
		General.AddComponentToEntity(
		    world, player_entity, new HealthBuff
		    {
			_health_increase = 0
		    }
		);
	    }

	    ref var health_buff = ref health_buff_pool.Get(player_entity);
	    health_buff._health_increase += health_potion._health_increase;
	}
    }
}
