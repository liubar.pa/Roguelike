using Leopotam.EcsLite;

namespace Client
{
    /// System which processes the usage of speed potions by the
    /// player
    sealed class SpeedPotionSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();
	    var inventory_pool = world.GetPool<PlayerInventory>();
	    var item_use_request_pool = world.GetPool<ItemUseRequest>();
	    var speed_potion_pool = world.GetPool<SpeedPotion>();
	    
	    int player_entity = General.GetFirstEntity<Controlable>(world);
	    ref var inventory = ref inventory_pool.Get(player_entity);

	    var item_use_requests = world
		.Filter<ItemUseRequest>().End();
	    foreach (var entity in item_use_requests)
	    {
		int slot_index = item_use_request_pool
		    .Get(entity)
		    ._slot_index;
		int? item = inventory._storage[slot_index];

		if (item is int item_entity &&
		    speed_potion_pool.Has(item_entity))
		{
		    HandleSpeedPotion(
			world, player_entity, item_entity
		    );
		}
	    }
        }

	/// Handles speed potion usage: updates player's current speed
	/// buff (creates one if it does not exist)
	void HandleSpeedPotion(EcsWorld world, int player_entity,
				int item_entity)
	{
	    var speed_potion_pool = world.GetPool<SpeedPotion>();
	    var speed_buff_pool = world.GetPool<SpeedBuff>();
	    var moving_pool = world.GetPool<Moving>();

	    ref var speed_potion = ref speed_potion_pool
		.Get(item_entity);

	    if (!speed_buff_pool.Has(player_entity))
	    {
		float old_speed = moving_pool
		    .Get(player_entity)
		    ._speed;
		General.AddComponentToEntity(
		    world, player_entity, new SpeedBuff
		    {
			_old_speed = old_speed,
			_new_speed = 0,
			_time_left = 0
		    }
		);
	    }

	    ref var speed_buff = ref speed_buff_pool.Get(player_entity);
	    speed_buff._new_speed =
		speed_buff._old_speed * speed_potion._multiplier;
	    speed_buff._time_left = speed_potion._duration;
	}
    }
}
