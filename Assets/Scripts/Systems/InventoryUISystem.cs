using System;
using Leopotam.EcsLite;
using Leopotam.EcsLite.Unity.Ugui;
using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace Client
{
    /// System which deals with whole inventory UI
    sealed class InventoryUISystem : IEcsInitSystem, IEcsRunSystem
    {
	EcsUguiEmitter _ugui_emitter;

	[EcsUguiNamed("Inventory")]
	Transform _inventory;

	public void Init(IEcsSystems systems)
	{
	    var world = systems.GetWorld();
	    var data = systems.GetShared<GameSharedData>();

	    CreateInventoryUI(world, data._prefabs);
	}

        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();

	    UpdateSlots(world);
        }

	/// Initializes inventory UI by adding and initializing all
	/// slots
	void CreateInventoryUI(EcsWorld world, GamePrefabs prefabs)
	{
	    // Getting inventory capacity
	    int capacity = General
		.GetFirstComponent<PlayerInventory>(world)
		._storage.Length;

	    // Getting prefab
	    var slot_prefab = prefabs._slot_prefab;

	    // Creating slots

	    int melee_slot = CreateSlot(world, slot_prefab);
	    int shooting_slot = CreateSlot(world, slot_prefab);

	    int[] storage_slots = new int[capacity];
	    for (int i = 0; i < capacity; ++i)
	    {
		storage_slots[i] = CreateSlot(world, slot_prefab);
	    }

	    // Creating inventory UI entity
	    var inventory_entity = world.NewEntity();
	    General.AddComponentToEntity(
		world, inventory_entity, new InventoryUI
		{
		    _melee_slot = melee_slot,
		    _shooting_slot = shooting_slot,
		    _storage_slots = storage_slots
		}
	    );
	}

	/// Creates and initializes new slot. Attaches it to inventory
	/// UI panel
	int CreateSlot(EcsWorld world, GameObject slot_prefab)
	{
	    // Instantiating object and adding it to UI
	    GameObject slot = GameObject.Instantiate(
		slot_prefab, Vector3.zero, Quaternion.identity
	    );
	    Transform slot_transform = slot.transform;
	    slot_transform.SetParent(_inventory, false);

	    // Getting necessary UI elements
	    Image background = slot.GetComponent<Image>();
	    Image item = slot_transform.Find("SlotItem")
		.GetComponent<Image>();

	    // Creating component for slot UI
	    InventorySlotUI slot_ui = new InventorySlotUI
	    {
		_background = background,
		_item = item
	    };

	    // Resetting slot
	    UpdateSlot(ref slot_ui, null, false);

	    // Creating new entity
	    var slot_entity = world.NewEntity();
	    General.AddComponentToEntity(world, slot_entity, slot_ui);

	    return slot_entity;
	}

	/// Updates slots' UIs
	void UpdateSlots(EcsWorld world)
	{
	    // Obtaining player's inventory and inventory UI
	    ref var inventory = ref General
		.GetFirstComponent<PlayerInventory>(world);
	    ref var inventory_ui = ref General
		.GetFirstComponent<InventoryUI>(world);

	    var inventory_slot_ui_pool = world.GetPool<InventorySlotUI>();

	    // Updating melee slot
	    ref var melee_slot = ref inventory_slot_ui_pool
		.Get(inventory_ui._melee_slot);
	    UpdateSlot(
		ref melee_slot, GetItem(world, inventory._melee),
		false
	    );

	    // Updating shooting slot
	    ref var shooting_slot = ref inventory_slot_ui_pool
		.Get(inventory_ui._shooting_slot);
	    UpdateSlot(
		ref shooting_slot, GetItem(world, inventory._shooting),
		false
	    );

	    // Updating storage slots
	    for (int i = 0; i < inventory._storage.Length; ++i) {
		int? item = inventory._storage[i];
		ref var slot = ref inventory_slot_ui_pool
		    .Get(inventory_ui._storage_slots[i]);
		bool is_selected = i == inventory._selected_index;
		UpdateSlot(ref slot, GetItem(world, item), is_selected);
	    }
	}

	/// Updates given slot's UI. Sets background depending on
	/// whether given slot is selected and sets foreground sprite
	/// to item's sprite (or makes it fully transparent if there
	/// is no item given)
	void UpdateSlot(ref InventorySlotUI slot_ui, Item? item, bool is_selected)
	{
	    slot_ui._background.color = is_selected
		? new Color(1, 1, 0, 0.2f)
		: new Color(1, 1, 1, 0.2f);
	    slot_ui._item.sprite = item?._sprite;
	    slot_ui._item.color = new Color(
		1, 1, 1, item == null ? 0 : 1
	    );
	}

	/// Returns item by entity or `null` if `item_entity` is `null`
	Item? GetItem(EcsWorld world, int? item_entity) {
	    if (item_entity == null) {
		return null;
	    }
	    return world.GetPool<Item>().Get((int)item_entity);
	}
    }
}
