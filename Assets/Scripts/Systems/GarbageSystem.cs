using UnityEngine;
using Leopotam.EcsLite;

namespace Client
{
    sealed class GarbageSystem : IEcsRunSystem
    {
        /*
        Finds all entitys with Garbage component and destroys them
        */
        private EcsFilter garbageFilter;
        private EcsFilter timeFilter;
        private EcsFilter gameObjectFilter;
        private EcsPool<DestroyAfterTime> time_pool;
        private EcsPool<GameObjectInfo> gameObjects;

        public void Run(IEcsSystems systems)
        {
            var world = systems.GetWorld();
            garbageFilter = world.Filter<Garbage>().End();
            timeFilter = world.Filter<DestroyAfterTime>().End();
            gameObjectFilter = world.Filter<Garbage>().Inc<GameObjectInfo>().End();
            gameObjects = world.GetPool<GameObjectInfo>();
            time_pool = world.GetPool<DestroyAfterTime>();
            IncreaseTime(world);
            DestroyGameObjects(world);
            DestroyEntities(world);
        }

        private void IncreaseTime(EcsWorld world)
        {
            foreach (var entity in timeFilter)
            {
                ref var time_info = ref time_pool.Get(entity);
                time_info._time_left -= 1;
                if (time_info._time_left == 0)
                    world.GetPool<Garbage>().Add(entity);
            }
        }

        private void DestroyGameObjects(EcsWorld world)
        {
            foreach (var entity in gameObjectFilter)
            {
                Object.Destroy(gameObjects.Get(entity)._game_object);
            }
        }

        private void DestroyEntities(EcsWorld world)
        {
            foreach (var entity in garbageFilter)
            {
                world.DelEntity(entity);
            }
        }
    }
}