using Leopotam.EcsLite;

namespace Client
{
    sealed class MapGenerationSystem : IEcsRunSystem
    {
        /*
        Process MapGenerationRequest-s. Add to _entity_for_map generated component Map.
        */
        public void Run(IEcsSystems systems)
        {
            //TODO.MapGeneration: implement
        }

        /*
        Generates entity with Map component and entitys with component MapGraphNode. The entity with 
        node component, where player curently is, should be in _current_node_entity. If the whole map can 
        be large and so you generate only some piece of it, there is edge nodes, so there be opportunity to 
        generate MapExtendRequest with those nodes and you will extend the map from that edge node.
        Return entity with Map component
        */
        private void GenerateMap(/*some arguments*/)
        {
            //TODO.MapGeneration: implement
        }
    }
}