using Leopotam.EcsLite;

namespace Client
{
    /// Helper class for obtaining all buff systems
    static class BuffSystems
    {
	/// Returns array of all buff systems
	public static IEcsSystem[] GetSystems()
	{
	    return new IEcsSystem[]
	    {
		new HealthBuffSystem(),
		new RegenerationBuffSystem(),
		new SpeedBuffSystem()
	    };
	}
    }
}
