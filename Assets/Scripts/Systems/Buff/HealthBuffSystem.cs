using Leopotam.EcsLite;

namespace Client
{
    /// System which processes all health buffs. Health buffs are
    /// one-shot, so all `HealthBuff`s are removed after running
    sealed class HealthBuffSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();
	    var health_buff_pool = world.GetPool<HealthBuff>();
	    var health_pool = world.GetPool<Health>();

	    // Increasing health
	    var valid_health_buffs = world
		.Filter<HealthBuff>().Inc<Health>().End();
	    foreach (var entity in valid_health_buffs)
	    {
		float increase = health_buff_pool
		    .Get(entity)
		    ._health_increase;
		ref var health = ref health_pool.Get(entity);
		health._health += increase;
	    }

	    // Removing buffs
	    var health_buffs = world.Filter<HealthBuff>().End();
	    foreach (var entity in health_buffs)
	    {
		health_buff_pool.Del(entity);
	    }
        }
    }
}
