using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    /// Systems which processes all regeneration buffs. Regeneration
    /// buffs last for specific time, so all expired
    /// `RegenerationBuff`s are removed after running
    sealed class RegenerationBuffSystem : IEcsRunSystem
    {
	public void Run(IEcsSystems systems)
	{
	    var world = systems.GetWorld();
	    var regeneration_buff_pool = world.GetPool<RegenerationBuff>();
	    var health_pool = world.GetPool<Health>();

	    // Updating time
	    var regeneration_buffs = world.Filter<RegenerationBuff>().End();
	    foreach (var entity in regeneration_buffs)
	    {
		ref var buff = ref regeneration_buff_pool.Get(entity);
		buff._time_left -= Time.deltaTime;
	    }

	    // Damage requesting
	    var valid_regeneration_buffs = world
		.Filter<RegenerationBuff>().Inc<Health>().End();
	    foreach (var entity in valid_regeneration_buffs)
	    {
		ref var buff = ref regeneration_buff_pool.Get(entity);
		ref var health = ref health_pool.Get(entity);

		var damage_request = world.NewEntity();
		General.AddComponentToEntity(
		    world, damage_request, new DamageRequest{
			_damage = -Time.deltaTime * buff._rate,
			_damaged_entity = entity
		    }
		);
	    }

	    // Removing expired buffs
	    regeneration_buffs = world.Filter<RegenerationBuff>().End();
	    foreach (var entity in regeneration_buffs)
	    {
		ref var buff = ref regeneration_buff_pool.Get(entity);

		if (buff._time_left <= 0)
		{
		    regeneration_buff_pool.Del(entity);
		}
	    }
	}
    }
}
