using Leopotam.EcsLite;
using UnityEngine;

namespace Client
{
    /// System which processes all speed buffs. Speed buffs last for
    /// specific time, so all expired `SpeedBuff`s are removed after
    /// running
    sealed class SpeedBuffSystem : IEcsRunSystem
    {        
        public void Run(IEcsSystems systems)
        {
	    var world = systems.GetWorld();
	    var speed_buff_pool = world.GetPool<SpeedBuff>();
	    var moving_pool = world.GetPool<Moving>();

	    // Updating time
	    var speed_buffs = world.Filter<SpeedBuff>().End();
	    foreach (var entity in speed_buffs)
	    {
		ref var buff = ref speed_buff_pool.Get(entity);
		buff._time_left -= Time.deltaTime;
	    }

	    // Increasing or restoring speed
	    var valid_speed_buffs = world
		.Filter<SpeedBuff>().Inc<Moving>().End();
	    foreach (var entity in valid_speed_buffs)
	    {
		ref var buff = ref speed_buff_pool.Get(entity);
		ref var moving = ref moving_pool.Get(entity);

		moving._speed = buff._time_left > 0
		    ? buff._new_speed
		    : buff._old_speed;
	    }

	    // Removing expired buffs
	    speed_buffs = world.Filter<SpeedBuff>().End();
	    foreach (var entity in speed_buffs)
	    {
		ref var buff = ref speed_buff_pool.Get(entity);

		if (buff._time_left <= 0)
		{
		    speed_buff_pool.Del(entity);
		}
	    }
        }
    }
}
