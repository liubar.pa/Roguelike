using UnityEngine;
using TMPro;

namespace Client {
    public class MerchantMenu : MonoBehaviour
    {
        public TMP_Text _player_money;
        public TMP_Text _merchant_money;
        public GameObject _selling_inventory;
        public GameObject _buying_inventory;
    }
}
