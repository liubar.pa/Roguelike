using UnityEngine;
using UnityEngine.UI;
using Leopotam.EcsLite;
using TMPro;

namespace Client {
    public class TradingItem : MonoBehaviour
    {
        public bool _is_player_owner;
        public TMP_Text _damage_text;
        public TMP_Text _price_text;
        public TMP_Text _name_text;
        public Image _image;
        public int _item_entity;
        public EcsWorld _world;
        public int _merchant_entity;
        public void CreateRequest()
        {
            int request_entity = _world.NewEntity();
            if (_is_player_owner) {
                General.AddComponentToEntity(_world, request_entity, new SellingRequest{
                    _item_entity=_item_entity,
                    _merchant_entity=_merchant_entity
                });
            } else {
                General.AddComponentToEntity(_world, request_entity, new BuyingRequest{
                    _item_entity=_item_entity,
                    _merchant_entity=_merchant_entity
                });
            }
        }
    }
}