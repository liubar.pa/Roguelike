using Leopotam.EcsLite;
using UnityEngine;
using UnityEditor;

namespace Client
{
    /// Helper functions for managing item entities
    class ItemUtils
    {
	/// Creates item entity
	public static int CreateItem(EcsWorld world, Item item)
	{
	    int item_entity = world.NewEntity();
	    General.AddComponentToEntity(world, item_entity, item);

	    return item_entity;
	}

	/// Converts item entity to "dropped" state. Item entity is
	/// considered to be in "dropped" state if the following and
	/// only the following components are attached to it: `Item`,
	/// `GameObjectInfo`, `Coordinate`, `Interactable`
	public static void MakeItemDropped(EcsWorld world,
					   GamePrefabs prefabs,
					   GameRules rules,
					   int item_entity,
					   Vector3 position)
	{
	    var item_pool = world.GetPool<Item>();

	    ref var item = ref item_pool.Get(item_entity);

	    var instance = GameObject.Instantiate(
		prefabs._item_prefab, position, Quaternion.identity
	    );

	    instance.GetComponent<SpriteRenderer>().sprite = item._sprite;

	    General.AddComponentToEntity(
		world, item_entity, new GameObjectInfo
		{
		    _game_object = instance,
		    _transform = instance.transform
		}
	    );
	    General.AddComponentToEntity(
		world, item_entity, new Coordinate
		{
		    _coordinate = position
		}
	    );
	    General.AddComponentToEntity(
		world, item_entity, new Interactable
		{
		    _interaction_distance = rules._item_interaction_distance,
		}
	    );
	}

	/// Converts item entity to "picked up" state. Item entity is
	/// considered to be in "picked up" state if the following and
	/// only the following components are attached to it: `Item`
	public static void MakeItemPickedUp(EcsWorld world,
					    int item_entity)
	{
	    var game_object_info_pool = world.GetPool<GameObjectInfo>();
	    var coordinate_pool = world.GetPool<Coordinate>();
	    var interactable_pool = world.GetPool<Interactable>();

	    GameObject.Destroy(
		game_object_info_pool.Get(item_entity)._game_object
	    );

	    game_object_info_pool.Del(item_entity);
	    coordinate_pool.Del(item_entity);
	    interactable_pool.Del(item_entity);
	}
    }
}
